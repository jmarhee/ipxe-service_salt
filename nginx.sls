nginx:
  pkg.installed

rm /var/www/html/index*:
  cmd.run

/var/www/html/index.html:
  file.managed:
    - source: salt://files/index.html
    - user: root
    - group: root