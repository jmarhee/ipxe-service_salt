extract_smartos:
  archive.extracted:
    - name: /var/www/html/smartos
    - archive_format: tar
    - source: https://us-east.manta.joyent.com/Joyent_Dev/public/SmartOS/platform-latest.tgz
    - user: root
    - group: root
    - skip_verify: True
    - if_missing: /var/www/html/smartos

/srv/omni-stable.iso:
  file.managed:
    - source: https://downloads.omniosce.org/media/lts/omniosce-r151022s.iso
    - user: root
    - group: root
    - skip_verify: True

mount -o loop /srv/omni-stable.iso /mnt/ && mkdir -p /var/www/html/omnios && cp -fr /mnt/* /var/www/html/omnios/ && umount /mnt:
  cmd.run

mv /var/www/html/smartos/platform* /var/www/html/smartos/platform:
  cmd.run

/var/www/html/smartos.ipxe:
  file.managed:
    - source: salt://files/smartos.ipxe
    - user: root
    - group: root

/var/www/html/omnios.ipxe:
  file.managed:
    - source: salt://files/omnios.ipxe
    - user: root
    - group: root
